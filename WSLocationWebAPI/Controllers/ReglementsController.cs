﻿using System;
using System.Collections.Generic;
using LocationLibrary.BusinessLogic;
using LocationLibrary.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace WSLocationWebAPI.Controllers
{
  [Route("api/v1/[controller]")]
  [ApiController]
  public class ReglementsController : SecureApiController
  {
    private readonly IReglementService _reglementService;
    private readonly ILogger<LocationsController> _logger;

    public ReglementsController(IReglementService ReglementService, ILogger<LocationsController> logger)
    {
      // Mise en oeuvre de l'Injection de Dépendance (voir Program.cs)
      _reglementService = ReglementService;
      // Gestion des logs
      _logger = logger;
    }

    // GET: api/<ReglementsController>
    /// <summary>
    /// Retourne la liste des règlements
    /// </summary>
    /// <returns>Une liste d'objet Reglement</returns>
    /// <see cref="Reglement"/>
    /// <example>
    /// http://serveur/api/v1/reglements
    /// </example>
    [HttpGet]
    [Authorize(Roles = "ROLE_ADMIN")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<List<Reglement>> Get()
    {
      try
      {
        return _reglementService.GetReglements();
      }
      catch (LocationException locationException)
      {
        _logger.LogError(locationException, locationException.Message);
        return StatusCode(locationException.StatusCode);
      }
      catch (Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
      }
    }

    // GET api/<ReglementsController>/5

    /// <summary>
    /// Retourne le règlement identifié par <paramref name="id"/>
    /// </summary>
    /// <param name="id">Id du réglement</param>
    /// <returns>Un objet Reglement</returns>
    /// <see cref="Reglement"/>
    /// <response code="404">Le règlement d'id id n'existe pas</response>     
    /// <example>
    /// http://serveur/api/v1/reglements/3
    /// </example>
    [HttpGet("{id}")]
    [Authorize]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<Reglement> GetReglement(int id)
    {
      try
      {
        Reglement? reglement = _reglementService.GetReglement(id);
        if (reglement == null)
        {
          return NotFound();
        }

        if (!SecurityCheckRoleAdminOrOwner(reglement.Location.Idutilisateur))
        {
          return Forbid();
        }
        
        return reglement;
      }
      catch (LocationException locationException)
      {
        _logger.LogError(locationException, locationException.Message);
        return StatusCode(locationException.StatusCode);
      }
      catch (Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
      }
    }

    // GET: api/<ReglementsController>/location/2
    /// <summary>
    /// Retourne la liste des règlements l'id d'une location
    /// </summary>
    /// <param name="idLocation">Id de la location</param>
    /// <returns>Une liste d'objet Reglement</returns>
    /// <see cref="Reglement"/>
    /// <example>
    /// http://serveur/api/v1/reglements/location/2
    /// </example>
    [HttpGet("location/{idLocation}")]
    [Authorize(Roles = "ROLE_ADMIN")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<List<Reglement>> GetReglementByLocation(int idLocation)
    {
      try
      {
        return _reglementService.GetReglementsByLocationId(idLocation);
      }
      catch (LocationException locationException)
      {
        _logger.LogError(locationException, locationException.Message);
        return StatusCode(locationException.StatusCode);
      }
      catch (Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
      }
    }


    // POST api/v1/reglements
    /// <summary>
    /// Ajout d'un réglement
    /// </summary>
    /// <param name="reglement">Objet réglement à créer</param>
    /// <see cref="Reglement"/>
    /// <returns></returns>
    [HttpPost]
    [Authorize(Roles = "ROLE_ADMIN")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<Reglement> AddReglement(Reglement reglement)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      Reglement? reglementCreated = null;
      try
      {
        reglementCreated = _reglementService.AddReglement(reglement);
      }
      catch (Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return BadRequest(exception.Message);
      }

      return CreatedAtAction(nameof(GetReglement), new { id = reglementCreated.Id }, reglementCreated);
    }

    // PUT api/v1/reglements/5
    /// <summary>
    /// Cette méthode n'est pas implémentée
    /// </summary>
    /// <param name="id"></param>
    /// <param name="reglement"></param>
    /// <returns>Toujours BadRequest</returns>
    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult UpdateReglement(int id, Reglement reglement)
    {
      return BadRequest("Vous ne pouvez pas modifier un règlement");
    }
    // DELETE api/v1/reglements/5
    /// <summary>
    /// Cette méthode n'est pas implémentée
    /// </summary>
    /// <param name="id"></param>
    /// <returns>Toujours BadRequest</returns>
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult DeleteReglement(int id)
    {
      return BadRequest("Vous ne pouvez pas supprimer un règlement");
    }
  }
}
