﻿using System;
using System.Collections.Generic;
using LocationLibrary.BusinessLogic;
using LocationLibrary.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.Extensions.Logging;


namespace WSLocationWebAPI.Controllers
{
  [Route("api/v1/[controller]")]
  [ApiController]
  public class RelancesController : SecureApiController
  {
    private readonly IRelanceService _relanceService;
    private readonly ILogger<LocationsController> _logger;

    public RelancesController(IRelanceService relanceService, ILogger<LocationsController> logger)
    {
      // Mise en oeuvre de l'Injection de Dépendance (voir Program.cs)
      _relanceService = relanceService;
      // Gestion des logs
      _logger = logger;
    }

    // GET: api/<RelancesController>
    /// <summary>
    /// Retourne la liste des relances
    /// </summary>
    /// <returns>Une liste d'objet Relance</returns>
    /// <see cref="Relance"/>
    /// <example>
    /// http://serveur/api/v1/relances
    /// </example>
    [HttpGet]
    [Authorize(Roles = "ROLE_ADMIN")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<List<Relance>> Get()
    {
      try
      {
        return _relanceService.GetRelances();
      }
      catch (LocationException locationException)
      {
        _logger.LogError(locationException, locationException.Message);
        return StatusCode(locationException.StatusCode);
      }
      catch (Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
      }
    }

    // GET api/<RelancesController>/5

    /// <summary>
    /// Retourne la relance identifiée par <paramref name="id"/>
    /// </summary>
    /// <param name="id">Id de la relance</param>
    /// <returns>Un objet Relance</returns>
    /// <see cref="Relance"/>
    /// <response code="404">La relance d'id id n'existe pas</response>     
    /// <example>
    /// http://serveur/api/v1/relances/3
    /// </example>
    [HttpGet("{id}")]
    [Authorize]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<Relance> GetRelance(int id)
    {
      try
      {
        Relance? relance = _relanceService.GetRelance(id);
        if (relance == null)
        {
          return NotFound();
        }

        if (!SecurityCheckRoleAdminOrOwner(relance.Location.Idutilisateur))
        {
          return Forbid();
        }
        return relance;
      }
      catch (LocationException locationException)
      {
        _logger.LogError(locationException, locationException.Message);
        return StatusCode(locationException.StatusCode);
      }
      catch (Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
      }
    }

    // GET: api/<RelancesController>/location/2
    /// <summary>
    /// Retourne la liste des relances d'après l'id d'une location
    /// </summary>
    /// <param name="idLocation">Id de la location</param>
    /// <returns>Une liste d'objet Relance</returns>
    /// <see cref="Relance"/>
    /// <example>
    /// http://serveur/api/v1/relances/location/2
    /// </example>
    [HttpGet("location/{idLocation}")]
    [Authorize(Roles = "ROLE_ADMIN")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<List<Relance>> GetRelanceByLocation(int idLocation)
    {
      try
      {
        return _relanceService.GetRelancesByLocationId(idLocation);
      }
      catch (LocationException locationException)
      {
        _logger.LogError(locationException, locationException.Message);
        return StatusCode(locationException.StatusCode);
      }
      catch (Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
      }
    }

    // POST api/v1/relances
    /// <summary>
    /// Ajout d'une relance
    /// </summary>
    /// <param name="relance">Objet relance à créer</param>
    /// <see cref="Relance"/>
    /// <returns>Le statut code 201 Created</returns>
    /// <response code="400">L'objet relance n'est pas conforme</response>    
    [HttpPost]
    [Authorize(Roles = "ROLE_ADMIN")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<Relance> AddRelance(Relance relance)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest();
      }
      Relance? relanceCreated = null;
      try
      {
        relanceCreated = _relanceService.AddRelance(relance);
      }
      catch(Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return BadRequest(exception.Message);
      }

      return CreatedAtAction(nameof(GetRelance), new { id = relanceCreated.Id }, relanceCreated);
    }

    // PUT api/v1/relances/5
    /// <summary>
    /// Cette méthode n'est pas implémentée
    /// </summary>
    /// <param name="id"></param>
    /// <param name="relance"></param>
    /// <returns>Toujours BadRequest</returns>
    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult UpdateRelance(int id, Relance relance)
    {
      return BadRequest("Vous ne pouvez pas modifier une relance");
    }

    // DELETE api/v1/relances/5
    [HttpDelete("{id}")]
    [Authorize(Roles = "ROLE_ADMIN")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult DeleteRelance(int id)
    {
      Relance? relance = null;
      try
      {
        // 1 - Recherche de la relance
        relance = _relanceService.GetRelance(id);
        if (relance == null)
        {
          return NotFound();
        }
        // 2 -Suppression
        _relanceService.DeleteRelance(id);
        _logger.LogInformation($"Suppression relance id={id}");
      }
      catch(Exception exception)
      {
        _logger.LogError(exception, exception.Message);
        return BadRequest(exception.Message);
      }

      return Ok(relance);
    }



  }
}
