using System.Security.Claims;
using LocationLibrary.Security;
using Microsoft.AspNetCore.Mvc;

namespace WSLocationWebAPI.Controllers;

public class SecureApiController : ControllerBase
{
    
    protected bool SecurityCheckRoleAdminOrUser()
    {
        if (User?.Identity == null)
            return false;

        /* La demande de modification est valide ssi 
         *  - le rôle est ROLE_SUPER_ADMIN
         *  ou 
         *  - le rôle est ROLE_USER
         */
        JwtAuthenticationIdentity? jwtAuthenticationIdentity = GetJWTIdentity();
        return jwtAuthenticationIdentity != null && (IsInRoleAdmin(jwtAuthenticationIdentity) || jwtAuthenticationIdentity.IsInRole("ROLE_USER"));
    }

    protected bool SecurityCheckRoleAdminOrOwner(string userLogin)
    {
        if (User?.Identity == null)
            return false;

        /* La demande de modification est valide ssi 
         *  - le rôle est ROLE_SUPER_ADMIN
         *  ou 
         *  - le rôle est ROLE_USER et l'utilisateur authentifié pat le token est l'utilisateur qui fait la modification
         */
        JwtAuthenticationIdentity? jwtAuthenticationIdentity = GetJWTIdentity();
        return jwtAuthenticationIdentity != null && (IsInRoleAdmin(jwtAuthenticationIdentity) || 
                                                     (jwtAuthenticationIdentity.IsInRole("ROLE_USER") && jwtAuthenticationIdentity.Name.Equals(userLogin, System.StringComparison.InvariantCultureIgnoreCase)));
    }

    protected JwtAuthenticationIdentity? GetJWTIdentity()
    {
        if (User?.Identity == null)
            return null;

        return AuthenticationModule.PopulateUser(User.Identity as ClaimsIdentity);
    }

    protected static bool IsInRoleAdmin(JwtAuthenticationIdentity jwtAuthenticationIdentity)
    {
        return jwtAuthenticationIdentity.IsInRole("ROLE_SUPER_ADMIN") ||
               jwtAuthenticationIdentity.IsInRole("ROLE_ADMIN");
    }
}