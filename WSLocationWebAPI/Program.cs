using System.IdentityModel.Tokens.Jwt;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

using NLog.Web;

using LocationLibrary.Models;
using LocationLibrary.BusinessLogic;
using LocationLibrary.Security;
using LocationLibrary.Utilitaires;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Prometheus;
using WSLocationWebAPI.Health;

var builder = WebApplication.CreateBuilder(args);

// NLog: Setup NLog for Dependency injection
builder.Logging.ClearProviders();
builder.Logging.SetMinimumLevel(LogLevel.Trace);
builder.Host.UseNLog();

// Avoid Json Loop
builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);


// Add services to the container.

// Database
builder.Services.AddDbContext<rhlocationContext>(options =>
    {
      var connectionString = builder.Configuration.GetConnectionString("RHLocationDatabase");
      options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
    }
);

// Dependency Injection
builder.Services.AddScoped<ISmtpGMail, SmtpGMail>(m => new SmtpGMail(builder.Configuration["Gmail:user"], builder.Configuration["Gmail:password"]));
builder.Services.AddScoped<ILocationService, LocationService>();
builder.Services.AddScoped<IRelanceService, RelanceService>();
builder.Services.AddScoped<IReglementService, ReglementService>();
builder.Services.AddScoped<IHabitationService, HabitationService>(x => new HabitationService(builder.Configuration["WSHabitationURI"]));

// JWT
// Réinitialise les clés pour les Claims:
// https://mderriey.com/2019/06/23/where-are-my-jwt-claims/
// Retirer v1.1
//JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

// JWT Implémentation
builder.Services.AddAuthentication(options =>
  {
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
  })
  .AddJwtBearer(options =>
  {
    // Configure JWT Bearer Auth to expect our security key
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters =
      new TokenValidationParameters
      {
        ValidateIssuer = false,
        ValidateAudience = false,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = AuthenticationModule.GetPublicKey(builder.Configuration["PublicKeyFileNamePath"])
      };
  }
   );

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
// Voir https://docs.microsoft.com/fr-fr/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-6.0&tabs=visual-studio
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
  c.SwaggerDoc("v1", new OpenApiInfo
  {
    Title = "WSLocations API",
    Version = "V1",
    Description = "API de gestion des locations en ASP.Net Core Web API",
    Contact = new OpenApiContact()
    {
      Name = "Jean-Luc Bompard AKA KilroyWasHere",
      Email = "kilroywashere.devops@gmail.com"
    }
  });

  // Ajout des commentaires XML
  // Set the comments path for the Swagger JSON and UI.
  var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
  var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
  c.IncludeXmlComments(xmlPath);
});

// Ajout des classes HealthCheck : step 1/2
// voir https://learn.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-7.0
builder.Services.AddHealthChecks()
  .AddCheck<LocationHealthCheck>("Location", tags: new[] { "ready" })
// DB Probe for Kubernetes
  .AddDbContextCheck<rhlocationContext>(tags:new [] { "dbready" });

var app = builder.Build();

app.UseRouting();

// Enabling CORS
app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader()
);

app.UseHttpsRedirection();


// Prometheus : Capture metrics about all received HTTP requests.
app.UseHttpMetrics();

// Ajout des classes HealthCheck : step 2/2
// voir : https://learn.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-6.0
app.MapHealthChecks("/healthz/readiness", new HealthCheckOptions
{
  Predicate = healthCheck => healthCheck.Tags.Contains("dbready")
});
app.MapHealthChecks("/healthz/liveness", new HealthCheckOptions
{
  Predicate = healthCheck => healthCheck.Tags.Contains("ready")
});

// Activation de l'authentification.
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.UseEndpoints(endpoints =>
{
  endpoints.MapControllers();
  // Prometheus : Enable /metrics page
  endpoints.MapMetrics();
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();

  app.UseSwaggerUI(c =>
  {
    // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
    // specifying the Swagger JSON endpoint : v1 est le nom défini avec SwaggerDoc (voir ConfigureServices)
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Gestions des locations V1");
  });
}


app.Run();
