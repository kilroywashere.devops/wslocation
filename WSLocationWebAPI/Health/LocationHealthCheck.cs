using LocationLibrary.BusinessLogic;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace WSLocationWebAPI.Health;

public class LocationHealthCheck: IHealthCheck
{
    private readonly ILocationService _locationService;
    
    public LocationHealthCheck(ILocationService locationService)
    {
        _locationService = locationService;
    }

    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        var isHealthy = false;

        try
        {
            _locationService.GetCount();
            isHealthy = true;
        }
        catch 
        {}

        if (isHealthy)
        {
            return Task.FromResult(
                HealthCheckResult.Healthy("Etat (Healthy) du web service WSLocationWebAPI"));
        }

        return Task.FromResult(
            new HealthCheckResult(
                context.Registration.FailureStatus, "Etat (failure) du web service WSLocationWebAPI"));
    }
}