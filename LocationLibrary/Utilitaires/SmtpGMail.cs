﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LocationLibrary.Utilitaires
{
  public class SmtpGMail : SmtpClient, ISmtpGMail
  {
    private readonly string email;

    public SmtpGMail(string email, string password)
        : base("smtp.gmail.com", 587)
    {
      EnableSsl = true;
      this.email = email;
      Credentials = new NetworkCredential(email, password);
    }

    public async Task<bool> SendMessageAsync(string to, string subject, string body)
    {
      try
      {
        MailMessage mail = new MailMessage(this.email, to, subject, body);
        mail.IsBodyHtml = true;

        await SendMailAsync(mail);
      }
      catch
      {
        return false;
      }

      return true;
    }
  }
}
