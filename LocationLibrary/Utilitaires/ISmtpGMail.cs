﻿namespace LocationLibrary.Utilitaires
{
  public interface ISmtpGMail
  {
    Task<bool> SendMessageAsync(string to, string subject, string body);
  }
}