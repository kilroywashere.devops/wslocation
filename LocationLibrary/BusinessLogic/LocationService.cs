﻿using LocationLibrary.Models;
using LocationLibrary.Utilitaires;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Text;

namespace LocationLibrary.BusinessLogic
{
  /**
   * 
   * REGENERATION du modèle :  dotnet ef -v  dbcontext scaffold "Server=192.168.6.10;Database=rhlocation;Uid=adminrh;Pwd=abcd4ABCD;" Pomelo.EntityFrameworkCore.MySql -o Models
   * 
   */

  public class LocationService : ILocationService
  {
    private readonly rhlocationContext contexte;
    private readonly IHabitationService habitationService;
    private readonly ISmtpGMail smtpGMail;

    public ILogger? Logger { get; set; }

    public LocationService(rhlocationContext contexte, IHabitationService habitationService, ISmtpGMail smtpGMail)
    {
      this.contexte = contexte;
      this.habitationService = habitationService;
      this.smtpGMail = smtpGMail;
    }

    public int GetCount()
    {
      return contexte.Locations.Count();
    }

    public List<Location> GetLocations()
    {
      return contexte.Locations
          .Include(f => f.Facture)
          .Include(r => r.Reglements)
          .Include(r => r.Relances)
          .ToList();
    }

    public List<Location> GetLocationsByUserId(string userId)
    {
      return contexte.Locations
        .Include(f => f.Facture)
        .Include(r => r.Reglements)
        .Include(r => r.Relances)
        .Where(loc => loc.Idutilisateur.Equals(userId))
        .ToList();
    }


    public Location? GetLocation(int id)
    {
      return contexte.Locations
          .Include(f => f.Facture)
          .Include(r => r.Reglements)
          .Include(r => r.Relances)
          .FirstOrDefault(l => l.Id == id);
    }

    public Location AddLocation(Location location)
    {
      try
      {
        Task<Location> task = _AddLocation(location);
        return task.Result;
      }
      catch (AggregateException ae)
      {
        throw ae.InnerException ?? new LocationException("Ajout location", ae);
      }
    }

    public Location? DeleteLocation(int id)
    {
      LogInfo($"Suppression de la location id={id}");
      // Recherche de la location
      Location? location = contexte.Locations.FirstOrDefault(l => l.Id == id);
      if (location == null)
        return null;

      DeleteLocation(location);
      return location;
    }

    public void DeleteLocation(Location location)
    {
      LogInfo($"Suppression de la location : {location.ToPlainText()}");
      // SI la date de début de location est avant la date du jour
      // ALORS la supression est possible
      // SINON on lance une exception
      if (location.Datedebut.Date.CompareTo(DateTime.Now) > 0)
      {
        LogError("Suppression de la location impossible car la location a déjà commencé");
        throw new LocationException($"Suppression de la location impossible car la location a déjà commencé: {location.ToPlainText()}");
      }

      // Les options de locations sont supprimées par la BDD
      contexte.Locations.Remove(location);
      try
      {
        contexte.SaveChanges();
        
        // Send mail to user or admin
        StringBuilder body = new StringBuilder();
        body.Append("Bonjour, <br /><br />");
        body.Append($"Vous venez de supprimer la location suivante : <br />{location.ToPlainText()}");
        body.Append(@"<br /><a href='mailto:rentahouse.kwh@gmail.com'>Rent A House</a>");

        smtpGMail.SendMessageAsync(location.Idutilisateur, "RentAHouse - Suppression d'une location", body.ToString());
      }
      catch (DbUpdateConcurrencyException ex)
      {
        LogError(ex, "Erreur dans la suppression de la location dans le référentiel");
        throw new LocationException($"Erreur dans la suppression de la location dans le référentiel : {location.ToPlainText()}", ex);
      }
    }
    
    

    private async Task<Location> _AddLocation(Location location)
    {
      LogInfo($"Ajout de la location : {location.ToPlainText()}");
      // 1 - Obtention de l'objet habitation
      Task<HabitationDto?> taskHabitationDto = habitationService.GetHabitationAsync(location.Idhabitation);

      // 2 - Vérification des contraintes (dates demandées, montant, ...)
      location.CheckAll();

      // 3 - Vérification des dates de locations existantes
      List<Location> locations = contexte.Locations.Where(l =>
          l.Idutilisateur == location.Idutilisateur &&
          l.Idhabitation == location.Idhabitation &&
          location.Datedebut <= l.Datefin && location.Datefin >= l.Datedebut
      ).ToList();
      if (locations.Count > 0)
      {
        LogError("Une location existe déjà pour les dates demandées");
        // Conflit de dates
        throw new LocationException("Une location existe déjà pour les dates demandées");
      }

      // 4 - Obtention de l'objet habitation
      // 4.1 - Appel du Web Service
      HabitationDto? habitationDto = await taskHabitationDto;
      if (habitationDto == null)
      {
        LogError($"L'habitation id={location.Idhabitation} n'existe pas");
        throw new LocationException($"L'habitation id={location.Idhabitation} n'existe pas");
      }
      // 4.2 Calcul du prix de base : Prix nuité * nombre de nuit
      location.Montanttotal = habitationDto.PrixNuit * location.GetDuration();
      
      // 5 - Ajout des options payantes de l'habitation
      AddOrUpdateOptionsPayantes(habitationDto.Optionpayantes);

      // 6 - Ajout de la location
      // 6.1 - Les options de la location doivent être présentes dans les options de l'habitation
      List<LocationOptionpayante> locationOptionpayantesValides = new List<LocationOptionpayante>();
      // 6.2 - L'objet Optionpayante de LocationOptionpayante doit être null
      foreach (LocationOptionpayante locationOptionpayante in location.LocationOptionpayantes)
      {
        // Vérification de la validitée des options
        if (locationOptionpayante.Optionpayante != null)
        {
          if (habitationDto.Optionpayantes.Find(hop => hop.Optionpayante.Id == locationOptionpayante.Optionpayante.Id) !=
              null)
          {
            locationOptionpayante.OptionpayanteId = locationOptionpayante.Optionpayante.Id;
            locationOptionpayante.Optionpayante = null;
            locationOptionpayantesValides.Add(locationOptionpayante);
            // Ajout du prix de l'option au prix total : Trigger dans la BDD
          }
        }
      }
      // 6.3 - On remplace les options de la location par celle valides
      location.LocationOptionpayantes = locationOptionpayantesValides;
      
      // 7 - Ajout dans la base de données
      contexte.Locations.Add(location);
      try
      {
        contexte.SaveChanges();
      }
      catch (DbUpdateConcurrencyException ex)
      {
        LogError(ex, $"Erreur dans l'ajout de la location dans le référentiel : {location.ToPlainText()}");
        throw new LocationException($"Erreur dans l'ajout de la location dans le référentiel : {location.ToPlainText()}", ex);
      }

      // 8 - Envoi du mail à l'utilisateur ou l'admin
      StringBuilder body = new StringBuilder();
      body.Append("Bonjour, <br /><br />");
      body.Append($"Vous venez d'ajouter la location suivante : <br />{location.ToPlainText()}");
      body.Append(@"<br /><a href='mailto:rentahouse.kwh@gmail.com'>Rent A House</a>");

      _ = smtpGMail.SendMessageAsync(location.Idutilisateur, "RentAHouse - Ajout d'une location", body.ToString());

      /*
       * 6 - Ajout des options payantes spécifique à la location
      bool locationOptionPayantesAdded = false;
      //  foreach (LocationOptionpayante locationOptionpayante in locationOptionpayante)
      foreach (LocationOptionpayante locationOptionpayante in location.LocationOptionpayantes)
      {
        // Ajout des options payantes de la location
        LocationOptionpayante newLocationOptionpayante = new LocationOptionpayante()
        {
          LocationId = location.Id,   // Id de la location nouvellement créée
          OptionpayanteId = locationOptionpayante.Optionpayante.Id,
          Prix = locationOptionpayante.Prix
        };
        contexte.LocationOptionpayantes.Add(newLocationOptionpayante);
        locationOptionPayantesAdded = true;
      }
      if (locationOptionPayantesAdded)
      {
        try
        {
          contexte.SaveChanges();
        }
        catch (DbUpdateConcurrencyException ex)
        {
          throw new LocationException($"Erreur dans l'ajout de la location dans le référentiel : {location.ToPlainText()}", ex);
        }
      }
      */

      return location;
    }

    private void AddOrUpdateOptionsPayantes(List<LocationOptionpayante> habitationDtoOptionpayantes)
    {
      bool isAdd = false;
      // Recherche des options payantes
      foreach (LocationOptionpayante locationOptionpayante in habitationDtoOptionpayantes)
      {
        if (locationOptionpayante.Optionpayante != null)
        {
          Optionpayante? optionpayante = contexte.Optionpayantes.Find(locationOptionpayante.Optionpayante.Id);
          if (optionpayante == null)
          {
            contexte.Optionpayantes.Add(locationOptionpayante.Optionpayante);
            isAdd = true;
          }
          
        }
      }


      try
      {
        if (isAdd)
        {
          contexte.SaveChanges();
        }
      }
      catch (DbUpdateConcurrencyException ex)
      {
        LogError(ex, "Erreur lors de l'ajout des options payantes dans le référentiel");
        throw new LocationException("Erreur lors de l'ajout des options payantes dans le référentiel", ex);
      }
    }

    private void LogInfo(string message)
    {
      Logger?.LogInformation(message);
    }

    private void LogError(string message)
    {
      Logger?.LogError(message);
    }

    private void LogError(Exception exception, string? message)
    {
      Logger?.LogError(exception, message == null ? message : exception.Message);
    }
  }
}
