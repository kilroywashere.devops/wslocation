﻿using LocationLibrary.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocationLibrary.Utilitaires;

namespace LocationLibrary.BusinessLogic
{
    public class RelanceService : IRelanceService
    {
        private static string dateRelanceSuperieureDateJour = "La date de relance doit être supérieure ou égale à la date du jour";
        
        private readonly rhlocationContext contexte;
        private readonly ISmtpGMail smtpGMail;

        public RelanceService(rhlocationContext contexte, ISmtpGMail smtpGMail)
        {
            this.contexte = contexte;
            this.smtpGMail = smtpGMail;
        }

        public List<Relance> GetRelances()
        {
            return contexte.Relances
                .Include(l => l.Location)
                .ToList();
        }

        public List<Relance> GetRelancesByLocationId(int idLocation)
        {
            return contexte.Relances
                .Include(l => l.Location)
                .Where((l => l.LocationId == idLocation))
                .ToList();
        }

        public Relance? GetRelance(int id)
        {
            return contexte.Relances
                .Include(l => l.Location)
                .FirstOrDefault(r => r.Id == id);
        }

        public Relance AddRelance(Relance relance)
        {
            if (relance.Date.CompareTo(DateTime.Now) < 0)
            {
                throw new LocationException(dateRelanceSuperieureDateJour);
            }

            contexte.Relances.Add(relance);
        
            try
            {
                contexte.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new LocationException($"Erreur dans l'ajout de la location dans le référentiel : {relance.Location.ToPlainText()}", ex);
            }
            // Send mail to user or admin
            StringBuilder body = new StringBuilder();
            body.Append("Bonjour, <br /><br />");
            body.Append($"Vous devez solder le compte pour la location suivante : <br />{relance.Location.ToPlainText()}");
            body.Append(@"<br /><a href='mailto:rentahouse.kwh@gmail.com'>Rent A House</a>");
            smtpGMail.SendMessageAsync(relance.Location.Idutilisateur, "RentAHouse - Relance pour le réglement d'une location", body.ToString());

            return relance;
        }

        public Relance? DeleteRelance(int id)
        {
            Relance? relance = GetRelance(id);
            if (relance == null)
                return null;

            DeleteRelance(relance);
            return relance;
        }

        public void DeleteRelance(Relance relance)
        {
            contexte.Relances.Remove(relance);
            try
            {
                contexte.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new LocationException($"Erreur dans la suppression de la location dans le référentiel : {relance.ToPlainText()}", ex);
            }
        }
    }
}
