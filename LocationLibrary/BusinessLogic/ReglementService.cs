﻿using LocationLibrary.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocationLibrary.Utilitaires;

namespace LocationLibrary.BusinessLogic
{
  public class ReglementService : IReglementService
  {
    private readonly rhlocationContext contexte;
    private static string dateVersementSuperieureDateJour = "La date de versement doit être supérieure ou égale à la date du jour";
    private readonly ISmtpGMail smtpGMail;

    public ReglementService(rhlocationContext contexte, ISmtpGMail smtpGMail)
    {
      this.contexte = contexte;
      this.smtpGMail = smtpGMail;
    }

    public List<Reglement> GetReglements()
    {
      return contexte.Reglements
          .Include(l => l.Location)
          .Include(t => t.Typereglement)
          .ToList();
    }

    public List<Reglement> GetReglementsByLocationId(int idLocation)
    {
      return contexte.Reglements
          .Include(l => l.Location)
          .Include(t => t.Typereglement)
          .Where(l => l.LocationId == idLocation)
          .ToList();
    }

    public Reglement? GetReglement(int id)
    {
      return contexte.Reglements
          .Include(l => l.Location)
          .Include(t => t.Typereglement)
          .FirstOrDefault(r => r.Id == id);
    }

    public Reglement AddReglement(Reglement reglement)
    {
      if (reglement.Dateversement.CompareTo(DateTime.Now) < 0)
      {
        throw new LocationException(dateVersementSuperieureDateJour);
      }
      
      contexte.Reglements.Add(reglement);
      try
      {
        contexte.SaveChanges();
      }
      catch (DbUpdateConcurrencyException ex)
      {
        throw new LocationException($"Erreur dans l'ajout d'un réglement dans le référentiel : {reglement.ToPlainText()}", ex);
      }
        
      // Send mail to user or admin
      StringBuilder body = new StringBuilder();
      body.Append("Bonjour, <br /><br />");
      body.Append($"Vous venez de fair eun réglement de {reglement.Montant:C} pour la location suivante : <br />{reglement.Location.ToPlainText()}");
      body.Append(@"<br /><a href='mailto:rentahouse.kwh@gmail.com'>Rent A House</a>");
      smtpGMail.SendMessageAsync(reglement.Location.Idutilisateur, "RentAHouse - Versement d'un règlement", body.ToString());

      return reglement;
    }
  }
}
