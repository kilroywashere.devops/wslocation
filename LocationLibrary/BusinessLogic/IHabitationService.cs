﻿using LocationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationLibrary.BusinessLogic
{
  public interface IHabitationService
  {
    //HabitationDto? GetHabitationOLD(int id);
    Task<HabitationDto?> GetHabitationAsync(int id);
  }
}
