﻿using LocationLibrary.Models;
using Microsoft.Extensions.Logging;

namespace LocationLibrary.BusinessLogic
{
  public interface ILocationService
  {
    public ILogger? Logger { get; set; }

    int GetCount();
    List<Location> GetLocations();
    List<Location> GetLocationsByUserId(string userId);
    Location? GetLocation(int id);
    Location AddLocation(Location location);
    Location? DeleteLocation(int id);
    void DeleteLocation(Location location);
  }
}