﻿using LocationLibrary.BusinessLogic;
using LocationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace LocationLibrary.BusinessLogic
{
  public class HabitationService : IHabitationService
  {
    private string uri;

    public HabitationService(string Uri)
    {
      this.uri = Uri;
    }

    public async Task<HabitationDto?> GetHabitationAsync(int id)
    {
      Console.WriteLine("1 - HabitationService.GetHabitation");
      String targetUri = $"{uri}/{id}";

      Console.WriteLine("2 - HabitationService.GetHabitation");
      using (HttpClient client = new HttpClient())
      {
        HttpResponseMessage responseMessage = await client.GetAsync(targetUri);
        if (responseMessage.IsSuccessStatusCode)
        {
          HabitationDto? habitationDto = await responseMessage.Content.ReadFromJsonAsync<HabitationDto>();
          Console.WriteLine("3.1 - HabitationService.GetHabitation");
          return habitationDto;
        }
        Console.WriteLine("3.2 - HabitationService.GetHabitation");
        return null;
      }
    }
  }
}
