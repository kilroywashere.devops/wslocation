using System.Security.Principal;

namespace LocationLibrary.Security;

public class JwtAuthenticationIdentity : GenericIdentity
    {

    public string Nom { get; set; }
    public string Prenom { get; set; }
    public string Mail { get; set; }
    public string[] Roles { get; set; }

    public JwtAuthenticationIdentity(string login)
        : base(login)
    {
        Roles = new[] {""};
        Nom = Prenom = Mail = "";
    }
    public JwtAuthenticationIdentity(string login, string roles)
        : base(login)
    {
        Roles = roles.Split(new char[] { ',' });
        Nom = Prenom = Mail = "";
    }

    public bool IsInRole(string role)
    {
        return Roles.Contains(role);
    }

    public GenericPrincipal GetPrincipal()
    {
        return new GenericPrincipal(this, Roles);
    }

}