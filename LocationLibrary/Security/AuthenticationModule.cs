    using LocationLibrary.Models;

    namespace LocationLibrary.Security;


    using System.IdentityModel.Tokens.Jwt;
    using Microsoft.IdentityModel.Tokens;
    using System.Security.Claims;
    using System.Security.Cryptography.X509Certificates;
    using System.Security.Cryptography;
    using System.IO;

    public static class AuthenticationModule
    {
        public static X509SecurityKey GetPublicKey(string fullPathName)
        {
            X509Certificate2 x509Certificate = LoadCertificateFile(fullPathName);
            X509SecurityKey publicKey = new X509SecurityKey(x509Certificate);

            return publicKey;
        }

        private static JwtAuthenticationIdentity PopulateUserFromClaims(IEnumerable<Claim> claims)
        {
            string login = claims.First(c => c.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            
            List<string> roles = new List<string>();
            foreach (var claim in claims)
            {
                if (claim.Type.Equals(ClaimTypes.Role))
                    roles.Add(claim.Value);
            }

            return new JwtAuthenticationIdentity(login)
            {
                Roles = roles.ToArray(),
                Nom = claims.First(c => c.Type.Equals("nom")).Value,
                Prenom = claims.First(c => c.Type.Equals("prenom")).Value,
                Mail = claims.First(c => c.Type.Equals("mail")).Value,
            };
        }
        
        public static JwtAuthenticationIdentity PopulateUser(ClaimsIdentity claimsIdentity)
        {
            return PopulateUserFromClaims(claimsIdentity.Claims); 
        }

        private static X509Certificate2 LoadCertificateFile(string filename)
        {
            Console.Out.WriteLine($"AuthenticationModule.LoadCertificateFile({filename}");

            try
            {
                using (FileStream fs = File.OpenRead(filename))
                {
                
                    byte[] rawData = new byte[fs.Length];
                    fs.Read(rawData, 0, rawData.Length);
                    if (rawData.Length > 0)
                    {
                        return new X509Certificate2(rawData);
                    }
                }
            }
            catch
            {
                // ignored
            }

            throw new LocationException("Le certificat n'a pas pu être chargé");
        }
    }