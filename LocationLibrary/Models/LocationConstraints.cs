﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationLibrary.Models
{
    public static class LocationConstraints
    {
    private static string dateDebutApresDateJour = "La date de début doit être après la date d'aujourdh'ui";
    private static string dateFinApresDateDebut = "La date de fin doit être après la date de début";
    private static string montantVerseInferieurMontantTotal = "Le montant versé doit être inférieur ou égal au montant total";

    public static string DateDebutApresDateJourMessage
    { get { return dateDebutApresDateJour; } }
    public static string DateFinApresDateDebutMessage
    { get { return dateFinApresDateDebut; } }
    public static string MontantVerseInferieurMontantTotalMessage
    { get { return montantVerseInferieurMontantTotal; } }

        public static void CheckDate(this Location location)
        {
            if (location.Datedebut.Date.CompareTo(DateTime.Now.Date) < 0)
            {
                throw new LocationException(dateDebutApresDateJour);
            }
            if (location.Datedebut.Date.CompareTo(location.Datefin.Date) >= 0)
            {
                throw new LocationException(dateFinApresDateDebut);
            }
        }
        public static void CheckMontant(this Location location)
        {
            if (location.Montanttotal - location.Montantverse < 0)
            {
                throw new LocationException(montantVerseInferieurMontantTotal);
            }
        }
        public static void CheckAll(this Location location)
        {
            location.CheckDate();
            location.CheckMontant();
        }
    }
}