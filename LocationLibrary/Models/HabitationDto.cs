namespace LocationLibrary.Models;

public class HabitationDto
{
  public int Id { get; set; }
  public String? Libelle { get; set; }
  public double PrixNuit { get; set; }

  public List<LocationOptionpayante> Optionpayantes { get; set; } = null!;
}