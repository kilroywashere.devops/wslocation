﻿using System;
using System.Collections.Generic;

namespace LocationLibrary.Models
{
    public partial class LocationOptionpayante
    {
        public int LocationId { get; set; }
        public int OptionpayanteId { get; set; }
        public double Prix { get; set; }

        public virtual Location? Location { get; set; } = null!;
        public virtual Optionpayante? Optionpayante { get; set; } = null!;
    }
}
