﻿using System;
using System.Collections.Generic;

namespace LocationLibrary.Models
{
    public partial class Facture
    {
        /// <summary>
        /// Doit être identique à location_id
        /// </summary>
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Adresse { get; set; } = null!;

        public virtual Location IdNavigation { get; set; } = null!;
    }
}
