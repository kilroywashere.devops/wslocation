﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationLibrary.Models
{
    public static class LocationExtensions
    {
        public static string ToPlainText(this Location location)
        {
            return $"Location - {location.Id}, User: {location.Idutilisateur}, Habitation: {location.Idhabitation}, " +
                   $"de {location.Datedebut.ToShortDateString()} à {location.Datefin.ToShortDateString()}";
        }
        public static string ToPlainText(this Reglement reglement)
        {
            object typereglement = reglement.Typereglement == null
                ? reglement.TypereglementId
                : reglement.Typereglement.Libelle;
            return $"Reglement - {reglement.Id}, LocationId: {reglement.LocationId}, Montant: {reglement.Montant}, " +
                   $"versé le {reglement.Dateversement.ToShortDateString()} ({typereglement})";
        }
        public static string ToPlainText(this Relance relance)
        {
            return $"Relance - {relance.Id}, LocationId: {relance.LocationId}, Motif: {relance.Motif} " +
                   $"le {relance.Date.ToShortDateString()}";
        }

        public static int GetDuration(this Location location)
        {
            return location.Datefin.Date.Subtract(location.Datedebut.Date).Days;
        }
    }
}
