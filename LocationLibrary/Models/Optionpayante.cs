﻿using System;
using System.Collections.Generic;

namespace LocationLibrary.Models
{
    public partial class Optionpayante
    {
        public Optionpayante()
        {
            LocationOptionpayantes = new HashSet<LocationOptionpayante>();
        }

        public int Id { get; set; }
        public string Libelle { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<LocationOptionpayante> LocationOptionpayantes { get; set; }
    }
}
