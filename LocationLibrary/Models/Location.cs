﻿using System;
using System.Collections.Generic;

namespace LocationLibrary.Models
{
    public partial class Location
    {
        public Location()
        {
            LocationOptionpayantes = new HashSet<LocationOptionpayante>();
            Reglements = new HashSet<Reglement>();
            Relances = new HashSet<Relance>();
            Idutilisateur = "";
        }

        public int Id { get; set; }
        public string Idutilisateur { get; set; }
        public int Idhabitation { get; set; }
        public DateTime Datedebut { get; set; }
        public DateTime Datefin { get; set; }
        public double Montanttotal { get; set; }
        public double Montantverse { get; set; }

        public virtual Facture? Facture { get; set; } = null!;
        public virtual ICollection<LocationOptionpayante> LocationOptionpayantes { get; set; }
        public virtual ICollection<Reglement> Reglements { get; set; }
        public virtual ICollection<Relance> Relances { get; set; }
    }
}
