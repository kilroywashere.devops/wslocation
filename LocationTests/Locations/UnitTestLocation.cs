﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LocationLibrary.Models;

namespace LocationTests.Locations
{
    public class UnitTestLocation
    {
        [Test]
        public void ConstraintOk_ShouldSuccess()
        {
            // Arrange
            Location location = TestsUtilitaires.getDummyLocation();

            // Act
            location.CheckDate();

            // Assert
            Assert.Pass();
        }

        [Test]
        public void ConstraintDateDebut_ShouldFail()
        {
            LocationException exception = Assert.Throws<LocationException>(
                delegate
                {
                    // Arrange
                    Location location = TestsUtilitaires.getDummyLocation();
                    location.Datedebut = DateTime.Now.AddDays(-1);

                    // Act
                    location.CheckDate();

                    // Assert : LocationException should be raised
                });

            // Assert
            Assert.That(exception.Message, Is.EqualTo(LocationConstraints.DateDebutApresDateJourMessage));
        }

        [Test]
        public void ConstraintDates_ShouldFail()
        {
            LocationException exception = Assert.Throws<LocationException>(
                delegate
                {
                    // Arrange
                    Location location = TestsUtilitaires.getDummyLocation();
                    location.Datedebut = location.Datefin.AddDays(1);

                    // Act
                    location.CheckDate();

                    // Assert : LocationException should be raised
                });

            // Assert
            Assert.That(exception.Message, Is.EqualTo(LocationConstraints.DateFinApresDateDebutMessage));
        }

        [Test]
        public void ConstraintMontants_ShouldFail()
        {
            LocationException exception = Assert.Throws<LocationException>(
                delegate
                {
                    // Arrange
                    Location location = TestsUtilitaires.getDummyLocation();
                    location.Montanttotal = 100;
                    location.Montantverse = location.Montanttotal + 1;

                    // Act
                    location.CheckMontant();

                    // Assert : LocationException should be raised

                });

            // Assert
            Assert.That(exception.Message, Is.EqualTo(LocationConstraints.MontantVerseInferieurMontantTotalMessage));
        }
    }
}
