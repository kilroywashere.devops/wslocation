using LocationLibrary.BusinessLogic;
using LocationLibrary.Models;
using LocationLibrary.Utilitaires;
using Microsoft.EntityFrameworkCore;
using Moq;
using System.Drawing.Text;

namespace LocationTests.Locations;

public class TestLocationService
{
  // private static int month = 7, dayStart = 1, dayEnd = 4;
  private readonly IQueryable<Location> dummyLocations;
  private readonly Mock<rhlocationContext> mockContext;
  private readonly Mock<DbSet<Location>> mockDbSetLocation;
  private readonly HabitationDto habitationDTO;
  private readonly Mock<IHabitationService> mockHabitationService;
  private readonly Mock<ISmtpGMail> mockSmtpGmail;
  private readonly int yearToTest = DateTime.Now.Year + 5;

  public TestLocationService()
  {
    mockContext = new Mock<rhlocationContext>();
    mockDbSetLocation = new Mock<DbSet<Location>>();

    dummyLocations = new List<Location> {
      new Location()
      {
        Id = 1,
        Idutilisateur = "jane.doe@mail.com",
        Idhabitation = 3,
        Datedebut = new DateTime(yearToTest, 7, 10),
        Datefin = new DateTime(yearToTest, 7, 14),
        Montanttotal = 100,
        Montantverse = 100,
      },
      new Location()
      {
        Id = 2,
        Idutilisateur = "john.doe@mail.com",
        Idhabitation = 4,
        Datedebut = new DateTime(yearToTest, 7, 10),
        Datefin = new DateTime(yearToTest, 7, 11),
        Montanttotal = 100,
        Montantverse = 100,
      },
    }.AsQueryable();
    habitationDTO = new HabitationDto()
    {
      Id = 3,
      Libelle = "Test",
      Optionpayantes = new List<LocationOptionpayante>() // Liste vide
    };
    mockHabitationService = new Mock<IHabitationService>();
    mockSmtpGmail = new Mock<ISmtpGMail>();
  }

  [SetUp]
  public void Setup()
  {
    mockDbSetLocation.As<IQueryable<Location>>().Setup(m => m.Provider).Returns(dummyLocations.Provider);
    mockDbSetLocation.As<IQueryable<Location>>().Setup(m => m.Expression).Returns(dummyLocations.Expression);
    mockDbSetLocation.As<IQueryable<Location>>().Setup(m => m.ElementType).Returns(dummyLocations.ElementType);
    mockDbSetLocation.As<IQueryable<Location>>().Setup(m => m.GetEnumerator()).Returns(dummyLocations.GetEnumerator());
    mockContext.Setup(c => c.Locations).Returns(mockDbSetLocation.Object);

    // mockHabitationService.Setup(h => h.GetHabitation(It.IsAny<int>())).Returns(Task.FromResult(habitationDTO));
    mockHabitationService.Setup(h => h.GetHabitationAsync(It.IsAny<int>())).ReturnsAsync(habitationDTO);
    mockSmtpGmail.Setup(s => s.SendMessageAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
  }

  [Test]
  public void GetAll_ShouldReturnOk()
  {
    // Arrange
    var service = new LocationService(mockContext.Object, mockHabitationService.Object, mockSmtpGmail.Object);

    // Act
    var locations = service.GetLocations();

    // Assert
    Assert.That(locations.Count, Is.EqualTo(2));
  }

  [Test]
  public void GetOne_ShouldReturnOk()
  {
    // Arrange
    var service = new LocationService(mockContext.Object, mockHabitationService.Object, mockSmtpGmail.Object);

    // Act
    var location = service.GetLocation(1);

    // Assert
    Assert.IsNotNull(location);
    Assert.That(location.Id, Is.EqualTo(1));
  }

  [Test]
  public void Add_ShouldReturnCreated()
  {
    // Arrange
    /*var mockContext = new Mock<rhlocationContext>();
    mockContext.Setup(c => c.Locations).Returns(mockDbSetLocation.Object);

    var mockContext = new Mock<rhlocationContext>();
    mockContext.Setup(c => c.Locations).Returns(mockDbSetLocation.Object);*/

    var service = new LocationService(mockContext.Object, mockHabitationService.Object, mockSmtpGmail.Object);
    var locationToAdd = new Location()
    {
      Id = 1,
      Idutilisateur = "jane.doe@mail.com",
      Idhabitation = 3,
      Datedebut = new DateTime(yearToTest, 7, 5),
      Datefin = new DateTime(yearToTest, 7, 7),
      Montanttotal = 100,
      Montantverse = 100,
    };

    // Act
    var location = service.AddLocation(locationToAdd);

    // Assert
    Assert.IsNotNull(location);
  }

  [Test, Sequential]
  public void AddExistingLocation_ShouldThrowException(
    [Values(10, 11, 9, 13, 1)] int dayStart,
    [Values(14, 13, 11, 15, 15)] int dayEnd)
  {
    // Arrange
    var service = new LocationService(mockContext.Object, mockHabitationService.Object, mockSmtpGmail.Object);
    var locationToAdd = new Location()
    {
      Id = 100,
      Idutilisateur = "jane.doe@mail.com",
      Idhabitation = 3,
      Datedebut = new DateTime(yearToTest, 7, dayStart),
      Datefin = new DateTime(yearToTest, 7, dayEnd),
      Montanttotal = 100,
      Montantverse = 100,
    };

    var exception = Assert.Throws<LocationException>(delegate
      {
        // Act
        service.AddLocation(locationToAdd);
      });

    // Assert
    Assert.That(exception.Message, Is.EqualTo("Une location existe déjà pour les dates demandées"));
  }
}