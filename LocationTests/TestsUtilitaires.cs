using System.Security.Claims;
using LocationLibrary.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LocationTests;

public static class TestsUtilitaires
{
  public static Location getDummyLocation()
  {
    return new Location()
    {
      Id = 1,
      Idutilisateur = "jane.doe@mail.com",
      Idhabitation = 3,
      Datedebut = DateTime.Now.AddDays(10),
      Datefin = DateTime.Now.AddDays(14),
      Montanttotal = 100,
      Montantverse = 100
    };
  }
  public static List<Location> getDummyLocations()
  {
    return new List<Location> {
      new Location()
      {
        Id = 1,
        Idutilisateur = "jane.doe@mail.com",
        Idhabitation = 3,
        Datedebut = DateTime.Now.AddDays(8),
        Datefin = DateTime.Now.AddDays(11),
        Montanttotal = 100,
        Montantverse = 100,
      },
      new Location()
      {
        Id = 2,
        Idutilisateur = "john.doe@mail.com",
        Idhabitation = 4,
        Datedebut = DateTime.Now.AddDays(12),
        Datefin = DateTime.Now.AddDays(19),
        Montanttotal = 420,
        Montantverse = 100,
      }
    };
  }

  public static ControllerContext AddDummyIdentity(string idUtilisateur, string roles = "ROLE_USER")
  {
    var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
    {
      new Claim(ClaimTypes.NameIdentifier, idUtilisateur),
      new Claim(ClaimTypes.Role, roles),
      new Claim("nom", "nom"),
      new Claim("prenom", "prenom"),
      new Claim("mail", idUtilisateur),
    }, "mock"));

    return new ControllerContext()
    {
      HttpContext = new DefaultHttpContext() { User = user}
    }; 
  }
}